﻿#include <iostream>
#include <fstream>

using namespace std;

class PhoneBook {
private:
    string line;
    string phone;
    ifstream ifs;
public:
    PhoneBook()
    {
        ifs.open("<Path to you file>");
    }

    ~PhoneBook()
    {
        if(ifs.is_open())
        {
            ifs.close();
        }
    }

    string getPhone(string n)
    {
        string name;

        if(ifs.is_open())
        {
            while (getline(ifs, name)) {
                if(name.find(n) == 0)
                {
                    this->phone = name.substr(n.size()+1, name.size());
                    this->line = name;
                }
            }
        }

        return this->phone;
    }
};