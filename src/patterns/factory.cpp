#include <iostream>
using namespace std;

class Product {
public:
    virtual void sayHi() = 0;
};
class ConcretProduct1: public Product {
public:
    void sayHi()
    {
        cout << "Hi 1" << endl;
    }
};

class ConcretProduct2: public Product {
public:
    void sayHi()
    {
        cout << "Hi 2" << endl;
    }
};

class Creator {
public:
    virtual Product* FactoryMethod() = 0;
};

class ConcreteCreator1: public Creator {
public:
    Product* FactoryMethod()
    {
        return new ConcretProduct1();
    }
};

class ConcreteCreator2: public Creator {
public:
    Product* FactoryMethod()
    {
        return new ConcretProduct2();
    }
};

void run_factory()
{

    ConcreteCreator1 c1;
    ConcreteCreator2 c2;

    c1.FactoryMethod()->sayHi();
    c2.FactoryMethod()->sayHi();

}

