#include <iostream>
using namespace std;

class MathProxy {
public:
    virtual void add() = 0;
    virtual double sub(double) = 0;
};

class Obj {
public:
    virtual double sub(double) = 0;
};

class ObjConc: public Obj {
public:
    double sub(double d)
    {
        return d + d;
    }
};

class Math: public MathProxy {
    Obj* obj;
public:
    void add() {
        obj = new ObjConc();
    }

    double sub(double i) {
        return obj->sub(i);
    }
};

