#include <iostream>
#include <map>

using namespace std;

class Strategy {
public:
    virtual void algorithm() = 0;
};

class ConcretStrategy1: public Strategy {
public:
    void algorithm()
    {
        cout << "Alg 1" << endl;
    }
};

class ConcretStrategy2: public Strategy {
public:
    void algorithm()
    {
        cout << "Alg 2" << endl;
    }
};

class Context {
private:
    map<string, Strategy*> strat;
public:
    void setStrategy(Strategy* st, string name) {
        strat[name] = st;
    }
    void contextMethod(int i) {
        switch (i) {
            case 1:
                strat["one"]->algorithm();
                break;
            case 2:
                strat["two"]->algorithm();
                break;
            default:
                cout << "no func" << endl;
                break;
        }
    }
};

void run_strategy() {
    Context ctx;
    ctx.setStrategy(new ConcretStrategy1(), "one");
    ctx.setStrategy(new ConcretStrategy2(), "two");

    ctx.contextMethod(2);
}

