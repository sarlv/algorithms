#include <iostream>
#include <vector>

using namespace std;

class Message {
public:
    virtual void handleEvent() = 0;
};

class Printer {
public:
    virtual void addPrint(Message* msg) = 0;
    virtual void removePrint(Message* msg) = 0;
    virtual void notify() = 0;
};

class PrintA: public Printer {
private:
    vector<Message*> msg;
public:
    void addPrint(Message* m) {
        msg.push_back(m);
    }
    void removePrint(Message* m) {}
    void notify() {
        for_each(msg.begin(), msg.end(), [](Message* o) {
            o->handleEvent();
        });
    }
};

class Counters: public Message {
public:
    void handleEvent()
    {
        cout << "Hi from counter department" << endl;
    }
};

