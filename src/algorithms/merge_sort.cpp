#import <iostream>
using namespace std;

template <class T>
void merge_sort(T& num)
{
    for(int i = 0; i < num.size(); i += 2)
    {
        for(int j = 0; j < num.size(); j+=i+2)
        {
            for(int u = 0; u < num.size() -1; ++u)
            {
                if (num[u] > num[u + 1]) {
                    auto tmp = num[u + 1];
                    num[u + 1] = num[u];
                    num[u] = tmp;
                }
            }
        }
    }
}
