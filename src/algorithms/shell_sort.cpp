#import <iostream>
using namespace std;

template <class T>
void shell_sort(T& num, int h = 1)
{
   /* This algorithm is quite efficient
    for medium-sized data sets as its
    average and worst case complexity are of Ο(n),
    where n is the number of items.*/

    int interval = h * 3 + 1;

    if(num.size() >= interval * 2) {
        for(int i = 0; i < num.size() / 2; ++i)
        {
            if(num[i] > num[i + interval])
            {
                auto tmp = num[i];
                num[i] = num[i + interval];
                num[i + interval] = tmp;
            }
        }
    }

    for(int i = 0; i < num.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            if (num[i] < num[j]) {
                auto tmp = num[i];
                num[i] = num[j];
                num[j] = tmp;
            }
        }
    }
}


