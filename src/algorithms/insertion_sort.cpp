#import <iostream>
using namespace std;

template <class T>
void insertion_sort(T& num)
{
    for(int i = 0; i < num.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            if (num[i] > num[j]) {
                auto tmp = num[i];
                num[i] = num[j];
                num[j] = tmp;
            }
        }
    }
}

