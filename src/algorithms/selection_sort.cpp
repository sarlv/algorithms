#import <iostream>
using namespace std;

template <class T>
void selection_sort(T& num)
{
    for(int i = 0; i < num.size(); ++i)
    {
        int lowest = i;

        for(int j = i + 1; j < num.size(); ++j)
        {
            if(num[i] > num[j]) lowest = j;
        }

        if (num[i] > num[lowest]) {
            auto tmp = num[i];
            num[i] = num[lowest];
            num[lowest] = tmp;
        }

    }
}
