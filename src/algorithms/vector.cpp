#include<iostream>
using namespace std;

class vector {
    int sz;
    double* elem;
    int space;
public:
    vector& operator=(vector&& a)
    {
        delete[] elem;
        elem = a.elem;
        sz = a.sz;
        a.elem = nullptr;
        a.sz = 0;

        return *this;
    }

    double& operator [](int n)
    {
        return elem[n];
    }
    double operator [](int n) const
    {
        return elem[n];
    }

//    vector& operator= (const vector& a)
//    {
//        double* p = new double[a.sz];
//        copy(a.elem, a.elem + a.sz, p);
//        delete[] elem;
//        elem = p;
//        sz = a.sz;

//        return this;
//    }

    vector(int s)
            :sz{s},
             elem{new double[s]}
    {
        for(int i = 0; i < s; ++i)
            elem[i] = 0;
    }
    vector(initializer_list<double> lst)
            : sz( lst.size() ),
              elem{ new double[sz] }
    {
        copy(lst.begin(), lst.end(), elem);
    }
    vector(const vector& arg)
            : sz{ arg.sz }, elem{ new double[arg.sz] }
    {
        copy(arg.elem, arg.elem + sz, elem);
    }
    ~vector()
    {
        delete[] elem;
    }
    int size() const { return sz; }
    double get(int n) const { return elem[n]; }
    void set(int n, double v) { elem[n] = v; }
    void reserve(int newalloc) {
        if(newalloc <= space) return;
        double* p = new double[newalloc];
        for(int i = 0; i < sz; ++i) {
            p[i] = elem[i];
            delete[] elem;
            elem = p;
            space = newalloc;
        }
    }
    int capacity() const { return space; }
    void resize(int newsize) {
        reserve(newsize);
        for(int i=sz; i<newsize; ++i) {
            elem[i] = 0;
        }
        sz = newsize;
    }
};
