#import <iostream>
using namespace std;

template <class T>
void bubble_sort(T& num)
{
    int i = num.size();

    while(i--) {
        for (int j = 0; j < i; ++j) {
            if (num[j] > num[j + 1]) {
                auto tmp = num[j + 1];
                num[j + 1] = num[j];
                num[j] = tmp;
            }
        }
    }
}
