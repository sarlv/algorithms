#include <iostream>
using namespace std;

static int l;
static int r;

class Node {
public:
    Node(int n, Node* l, Node* r)
    {
        this->num = n;
        this->left = l;
        this->right = r;
    }
    int getValue()
    {
        return this->num;
    }
    Node* getLeft()
    {
        return this->left;
    }
    Node* getRight()
    {
        return this->right;
    }
    static int count(Node* root)
    {

        if(root->getLeft() != NULL)
        {
            ++l;
            count(root->getLeft());
        }
        if(root->getRight() != NULL)
        {
            ++r;
            count(root->getRight());
        }
        return (l > r) ? l : r ;
    }
    static bool search(int n, Node* root)
    {
        if(root == NULL)
        {
            return false;
        }
        if(n == root->getValue())
        {
            return true;
        }

        if(n < root->getValue())
        {
            return search(n, root->getLeft());
        }
        if(n > root->getValue())
        {
            return search(n, root->getRight());
        }

        return false;
    }

private:
    int num;
    Node* left;
    Node* right;
};