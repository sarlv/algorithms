#import <iostream>
#include <vector>
#include "src/algorithms/bubble_sort.cpp"
#include "src/algorithms/insertion_sort.cpp"
#include "src/algorithms/selection_sort.cpp"
#include "src/algorithms/merge_sort.cpp"
#include "src/algorithms/shell_sort.cpp"
#include "src/algorithms/binarytree.cpp"

#include "src/patterns/observer.cpp"
#include "src/patterns/proxy.cpp"
#include "src/other/phonebook.cpp"

using namespace std;

int main() {

//    vector<int> arr{14,33,27,35, 10,12,38,51, 67,81,11,34};
//    bubble_sort(arr);
    //insertion_sort(arr);
    //selection_sort(arr);
    //merge_sort(arr);
    //shell_sort(arr);

//    for(auto i : arr)
//        cout << i << endl;


//    Node* n1 = new Node(2, NULL, NULL);
//    Node* n2 = new Node(4, NULL, NULL);
//    Node* n3 = new Node(5, n1, n2);
//    cout << Node::search(2, n3) << endl;
//    cout << Node::count(n3) << endl;

//    Printer* print = new PrintA();
//    Message* count = new Counters();
//    print->addPrint(count);
//    print->notify();

    MathProxy* math = new Math();
    math->add();
    cout << math->sub(5.7) << endl;

//    unique_ptr<PhoneBook> phone(new PhoneBook());
//    cout << phone->getPhone("Michael") << endl;

    return 0;
}